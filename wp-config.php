<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'optimal_smartphones_db' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'Mpqi&#-Hf[TU`gg9sNcQ9^[_zjr[|JuN|aw:V^B)Iw7O#/JbPho7$8eBs|^C4N1D' );
define( 'SECURE_AUTH_KEY',  '8YG<uxq0:-=B]N<b{OZ8N(D:eS7OU?#I~wS-NV:MZev+Dawzsx!H-y$maEr*i Rl' );
define( 'LOGGED_IN_KEY',    '=MbQ/>~j4t8/n)mj=|TA*KeC34 h5wza`%yf=2Ho6Sbu-?h3[OSkj6WI0tK^sLht' );
define( 'NONCE_KEY',        '(SsD@|u<,?RsDSn%&?pI1%2SmC`C@ceA]eds-`h[+p`f}CW_7v!n$X3!apL2@eTA' );
define( 'AUTH_SALT',        '#z*+VB^.dElTd9l]%I)jAWtF;%/Dj_M%o_,%Prm7q@XU[G!ar]4T]ud1*(mSBwoW' );
define( 'SECURE_AUTH_SALT', 'L2lNOS?.iE#&o+ye&FwE9oqj[KL>m1Gz5gx,+uhtMBBByU!$!Zhnkj87I.9^<d5X' );
define( 'LOGGED_IN_SALT',   'uB|Q@|k{1)Y2=c,Zp#^kNHE&I/f;7vA`wJ)vj}-}qxj9SiJx6(&Ac9;lmM={WyvK' );
define( 'NONCE_SALT',       'n[a]?=:{5<=n`*C>hXIBJ[nN<!F^^e.Va)]>T!n9Z?EY&^(h=!>^=Ai4yIP+3bXx' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );
/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
