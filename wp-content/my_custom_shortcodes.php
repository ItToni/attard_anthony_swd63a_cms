<?php

function apiText(){

    return '<p>This API returns a list of smartphones from the database!</p>';
}

function image()
{
    $uri = get_stylesheet_directory_uri() . '/images/smartphonesImg.png';
    $uri = "'$uri'";
    $img = "<img src= $uri />";

    return $img;
}

function recentPost($atts, $content=null){

    $getpost = get_posts( array('number' => 1) );

    $getpost = $getpost[0];

    $return = '<p style="font-weight: bold">Recent Post: </p>'.$getpost->post_title;

    $return .= "<br /><a href='" . get_permalink($getpost->ID) . "'><em>read more →</em></a>";

    return $return;
}

function table() {

    $table =
    '<br /><br /> <h2>Top Brands</h2>
        <table>
          <tr>
            <th>No 1</th>
            <th>No 2</th>
            <th>No 3</th>
          </tr>
          <tr>
            <td>Samsung</td>
            <td>Apple</td>
            <td>Google</td>
          </tr>
        </table>';

    return $table;
}

function slider(){

    $slider1 = get_stylesheet_directory_uri().'/images/slider1.jpg';
    $slider2 = get_stylesheet_directory_uri().'/images/slider2.jpg';
    $slider3 = get_stylesheet_directory_uri().'/images/slider3.jpg';

    $slider1 = "'$slider1'";
    $slider2 = "'$slider2'";
    $slider3 = "'$slider3'";

    $html =
        "<meta name='viewport' content='width=device-width, initial-scale=1'>
         <link rel='stylesheet' href='https://www.w3schools.com/w3css/4/w3.css'>
        <style>
            .mySlides {display:none;}
        </style>
        <h2 class='w3-center'>Slideshow</h2>

        <div class='w3-content w3-display-container'>
          <img class='mySlides' src= $slider1 style='width:100%'>
          <img class='mySlides' src= $slider2 style='width:100%'>
          <img class='mySlides' src= $slider3 style='width:100%'>
        
          <button class='w3-button w3-black w3-display-left' onclick='plusDivs(-1)'>&#10094;</button>
          <button class='w3-button w3-black w3-display-right' onclick='plusDivs(1)'>&#10095;</button>
        </div>
        
        <script>
            var slideIndex = 1;
            showDivs(slideIndex);
            
            function plusDivs(n) {
              showDivs(slideIndex += n);
            }
            
            function showDivs(n) {
              var i;
              var x = document.getElementsByClassName('mySlides');
              if (n > x.length) {slideIndex = 1}
              if (n < 1) {slideIndex = x.length}
              for (i = 0; i < x.length; i++) {
                x[i].style.display = 'none';  
              }
              x[slideIndex-1].style.display = 'block';  
            }
        </script>";

    return $html;
}

function video(){

    $video =
    "<iframe width='875' height='438' src='https://www.youtube.com/embed/aeQnQ-vjtuY' 
    frameborder='0' allow='accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture' 
    allowfullscreen></iframe>";

    return $video;

}

function download(){

    $img = get_stylesheet_directory_uri().'/images/download.jpg';

    $img = "'$img'";

    $download =
    "<h1>Download Image</h1>

    <p>Click on the image to download it:<p>
    <a href=$img download>
      <img src=$img width='300' height='300'>
    </a>";

    return $download;
}

?>