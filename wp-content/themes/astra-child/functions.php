<?php
include(WP_CONTENT_DIR.'/my_custom_shortcodes.php');
add_shortcode( 'api_text_sc', 'apiText' );
add_shortcode( 'img_sc', 'image' );
add_shortcode( 'recent_post_sc', 'recentPost' );
add_shortcode( 'table_sc', 'table' );
add_shortcode( 'slider_sc', 'slider' );
add_shortcode( 'video_sc', 'video' );
add_shortcode( 'download_sc', 'download' );

define( 'CHILD_THEME_ASTRA_CHILD_VERSION', '1.0.0' );

function child_enqueue_styles() {

	wp_enqueue_style( 'astra-child-theme-css', get_stylesheet_directory_uri() . '/style.css'
        , array('astra-theme-css'), CHILD_THEME_ASTRA_CHILD_VERSION, 'all' );

}

add_action( 'wp_enqueue_scripts', 'child_enqueue_styles', 15 );